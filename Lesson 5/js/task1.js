/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
var CommentsArray = [];
// var myPrototype = {
//   avatarUrl: "img/avatar.jpg",
//   likeAdd: function (span) {
//       console.log('like add:', this);
//       this.likeCount++;
//       CommentsFeed( CommentsArray );
//   }
// }



function Comment(name, text, avatarUrl) {
  this.name = name;
  this.text = text;
  if( avatarUrl !== undefined){
    this.avatarUrl = avatarUrl;
  }
  this.likeCount = 0;
  CommentsArray.push( this );
}


Comment.prototype.avatarUrl = "img/avatar.jpg";
Comment.prototype.likeAdd = function (span) {
  this.likeCount++;
  span.innerText = this.likeCount;
}

function CommentsFeed(CommentsArray) {
  let CommentsFeed = document.getElementById("CommentsFeed");
  for (let i = 0; i < CommentsArray.length; i++) {
    let div = document.createElement("div");
    let name = document.createElement("span");
    let text = document.createElement("span");
    let avatar = document.createElement("img");
    let likeAdd = document.createElement("button");
        likeAdd.innerText = "Like";

    let likeCount = document.createElement("span");
    console.log( CommentsArray[i] );
    let bind = CommentsArray[i].likeAdd.bind( CommentsArray[i], likeCount );

        likeAdd.addEventListener('click', bind);

        avatar.src = CommentsArray[i].avatarUrl
    
    avatar.style.width = '24px';
    avatar.style.height = '24px';
    name.innerText = CommentsArray[i].name;
    text.innerText = CommentsArray[i].text;
    likeCount.innerText = CommentsArray[i].likeCount;
    div.classList.add("comment");
    div.appendChild(avatar);
    name.classList.add("comment__name");
    div.appendChild(name);
    div.appendChild(document.createElement("br"));
    text.classList.add("comment__text");
    div.appendChild(text);
    div.appendChild(document.createElement("br"));
    div.appendChild(likeAdd);
    div.appendChild(likeCount);
    CommentsFeed.appendChild(div);
  }
}

document.addEventListener("DOMContentLoaded", function () {
  new Comment("Ламер", "Как пропатчить KDE2 под FreeBSD?", "img/avatar1.png");
  new Comment("Медвед", "аффтар пеши исчо", "img/avatar2.png");
  new Comment("moderator", "бНОПНЯ", "img/avatar3.png");
  new Comment("Марк Цукерберг", "Дуров верни стену!");

  // let i = 0;
  // while(i < 1000) {
  //   arr = arr.concat(CommentsArray);
  //   i++;
  // }
  CommentsFeed( CommentsArray );
});

