let url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";
let urlFriends = "http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2";

const convertToJson = res => res.json();

const getRandomFromArray = array => array[Math.floor(Math.random() * array.length)];

fetch(url)
    .then(convertToJson)
    .then(res => {
        let random = getRandomFromArray(res);
        console.log('json', random);
        return random;
    })
    .then(res => {
        return fetch(urlFriends)
            .then(convertToJson)
            .then(resFriends => {
                let random = getRandomFromArray(resFriends);
                console.log('friendsList', random.friends);
                return {
                    username: res.name,
                    friend: random.friends
                }
            });
        })
    .then(res => {
        console.log('after combine', res);
        const target = document.getElementById("target");
        
        let div = document.createElement("div");
        div.innerHTML = `
        <div>
            <h2>${res.username}</h2>
            <ul>
            ${
                res.friend.map(friend => `<li>${friend.name}</li>`).join('')
            }
            </ul>
        </div>`;
        target.appendChild(div);
    });