async function getCompanies() {
    const response = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
    const companies = await response.json();
    return companies;
};
var companies = getCompanies();
companies.then(data => {
    let table = document.createElement("table");
    table.border = "1";
    for(let i = 0; i < data.length; i++) {
        let tr = document.createElement("tr");
        let tdCompany = document.createElement("td");
        let tdBalance = document.createElement("td");
        let tdDate = document.createElement("td");
        let tdAddress = document.createElement("td");
        let aDate = document.createElement("a");
        let aAddress = document.createElement("a");
        aDate.href = "#";
        aDate.innerText = "Показать дату регистрации";
        aDate.onclick = function(e) {
            e.preventDefault();
            tdDate.innerText = data[i].registered;
        };
        aAddress.href = "#";
        aAddress.innerText = "Показать адрес";
        aAddress.onclick = function(e) {
            e.preventDefault();
            tdAddress.innerText = `City ${data[i].address.city}\n
                                   Zip ${data[i].address.zip}\n
                                   Country ${data[i].address.country}\n
                                   State ${data[i].address.state}\n
                                   Street ${data[i].address.street}\n
                                   House ${data[i].address.house}`;
        }
        tdDate.appendChild(aDate);
        tdAddress.appendChild(aAddress);
        tdCompany.innerText = data[i].company;
        tdBalance.innerText = data[i].balance;
        tr.appendChild(tdCompany);
        tr.appendChild(tdBalance);
        tr.appendChild(tdDate);
        tr.appendChild(tdAddress);
        table.appendChild(tr);
    }
    document.body.appendChild(table);
});