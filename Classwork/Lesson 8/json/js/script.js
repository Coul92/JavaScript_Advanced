document.addEventListener('DOMContentLoaded', function() {
    let btnJsonStringify = document.getElementById("btnJsonStringify");
    let btnJsonParse = document.getElementById("btnJsonParse");
    let txtJsonParse = document.getElementById("txtJsonParse");
    btnJsonStringify.onclick = function() {
        let arr = [];
        for(let i = 0; i < document.forms.length; i++) {
            let form = document.forms[i];
            let obj = {};
            for (let j = 0; j < form.elements.length; j++) {
                let element = form.elements[j];
                if (element.type === 'text') {
                    obj[element.name] = element.value;
                }
            }
            arr.push(obj);
        }
        console.log(JSON.stringify(arr));
    };
    btnJsonParse.onclick = function() {
        console.log(JSON.parse(txtJsonParse.value));
    };
});