/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
    1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
    1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();

*/

let colors = {
    backgroundColor: 'purple',
    color: 'blue'
};

function func1(color) {
    document.body.style.backgroundColor = this.backgroundColor;
    document.body.style.color = color;
}

func1.call(colors, 'yellow');

function func2() {
    document.body.style.backgroundColor = this.backgroundColor;
    document.body.style.color = this.color;
}

var f = func2.bind(colors);
f();

function func3(innerText) {
    let h1 = document.createElement("h1");
    h1.innerText = innerText;
    document.body.appendChild(h1);
}
func3.apply(null, ["Lorem ipsum"]);


