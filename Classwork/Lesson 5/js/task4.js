/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/

function encryptCesar(offset, textDecrypted) {
  let textEncrypted = '';
  for(let i = 0; i < textDecrypted.length; i++) {
    let symbol = textDecrypted[i];
    let code = +(symbol.charCodeAt(0));
    //console.log(symbol, code);
    code += offset;
    symbol = String.fromCharCode(code);
    //console.log(symbol, code);
    textEncrypted += symbol;
  }
  return textEncrypted;
}
function decryptCesar(offset, textEncrypted) {
  let textDecrypted = '';
  for(let i = 0; i < textEncrypted.length; i++) {
    
    let symbol = textEncrypted[i];
    let code = +(symbol.charCodeAt(0));
    //alert(symbol);
    //console.log(symbol, code);
    code -= offset;
    symbol = String.fromCharCode(code);
    //console.log(symbol, code);
    textDecrypted += symbol;
  }
  return textDecrypted;
}
var encryptCesar1 = encryptCesar.bind(null, 1);
var encryptCesar2 = encryptCesar.bind(null, 2);
var encryptCesar3 = encryptCesar.bind(null, 3);
var encryptCesar4 = encryptCesar.bind(null, 4);
var encryptCesar5 = encryptCesar.bind(null, 5);
var decryptCesar1 = decryptCesar.bind(null, 1);
var decryptCesar2 = decryptCesar.bind(null, 2);
var decryptCesar3 = decryptCesar.bind(null, 3);
var decryptCesar4 = decryptCesar.bind(null, 4);
var decryptCesar5 = decryptCesar.bind(null, 5);
let text = 'ABC';
let offset = 1;
let textEncrypted = encryptCesar(offset, text);
let textDecrypted = decryptCesar(offset, textEncrypted);
console.log(text, textEncrypted, textDecrypted);
textEncrypted = encryptCesar2(text);
textDecrypted = decryptCesar2(textEncrypted);
console.log(text, textEncrypted, textDecrypted);
