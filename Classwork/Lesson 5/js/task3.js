/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

function Dog(name, breed) {
  this.name = name;
  this.breed = breed;
  this.run = function() {
    this.status = 'run';
  }
  this.eat = function() {
    this.status = 'eat';
  };
  this.sleep = function() {
    this.status = 'sleep';
  }
}

let Lassie = new Dog('Lassie', 'Collie');
Lassie.run();
Lassie.status = 'flying';
console.log(Lassie.status);