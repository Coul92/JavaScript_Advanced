/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

let Train = {
    name: 'Hogwarts Express',
    speed: 0,
    passengersCount: 0,
    speedMeasure : 'км/ч',
    move : function() {
        this.speed = 100;
        console.log('Поезд' + this.name + ' везет ' + this.passengersCount + ' пассажиров со скоростью ' + this.speed + ' ' + this.speedMeasure);
    },
    stop : function() {
        this.speed = 0;
        console.log('Поезд' + this.name + ' остановился. Скорость ' + this.speed + ' ' + this.speedMeasure);
    },
    passengersAdd : function(passengersCount) {
        this.passengersCount += passengersCount;
        console.log('Поезд ' + this.name + ' подобрал' + passengersCount + ' пассажиров, теперь в поезде ' + this.passengersCount + ' пассажиров');
    }
};

Train.passengersAdd(100);
Train.move();
Train.stop();
Train.passengersAdd(50);
Train.move();
