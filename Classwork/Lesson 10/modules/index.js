let birdCarryPost = require('./birdCarryPost');
let birdEat = require('./birdEat');
let birdFly = require('./birdFly');
let birdHunt = require('./birdHunt');
let birdLayEgg = require('./birdLayEgg');
let birdRun = require('./birdRun');
let birdSing = require('./birdSing');
let birdSwim = require('./birdSwim');

module.exports = { birdCarryPost, birdEat, birdFly, birdHunt, birdLayEgg, birdRun, birdSing, birdSwim };