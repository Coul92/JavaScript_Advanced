/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */

 let modules = require('./modules');

 class Bird {
   constructor(name) {
     this.name = name;
     this.birdCarryPost = modules.birdCarryPost;
     this.birdEat = modules.birdEat;
     this.birdFly = modules.birdFly;
     this.birdHunt = modules.birdHunt;
     this.birdLayEgg = modules.birdLayEgg;
     this.birdRun = modules.birdRun;
     this.birdSing = modules.birdSing;
     this.birdSwim = modules.birdSwim;
   }
 }

 var bird = new Bird("Страус");
 console.log(bird);
 bird.birdFly();
